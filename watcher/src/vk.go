package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type VkUserList struct {
	WatchedUserList
}

type LastSeen struct {
	Time     int64
	Platform int8
}

type VkUser struct {
	Id         int64
	First_name string
	Last_name  string
	Online     int8
	Hidden     int8
	Last_seen  LastSeen
}

type VkResponse struct {
	Response []VkUser
}

func (response VkResponse) Convert() (*[]BaseUserData, error) {
	result := &[]BaseUserData{}

	for _, item := range response.Response {
		*result = append(*result, BaseUserData{
			Id:         item.Id,
			Online:     item.Online,
			LastOnline: item.Last_seen.Time,
		})
	}
	return result, nil
}

func (list VkUserList) StatusesLoad() (SocialResponse, error) {

	identitiesParts, err := list.GetIdentitiesParts()
	if err != nil {
		return nil, err
	}

	response := VkResponse{}
	urlBase := "https://api.vk.com/method/users.get?fields=online,last_seen&v=5.37&user_ids="
	for _, identities := range identitiesParts {
		url := urlBase + identities
		users, err := list.request(url)
		if err != nil {
			return nil, err
		}
		response.Response = append(response.Response, *users...)
	}

	return response, nil
}

func (list VkUserList) request(url string) (*[]VkUser, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	var parsed VkResponse
	err = json.Unmarshal(data, &parsed)
	if err != nil {
		return nil, err
	}

	return &parsed.Response, nil
}
