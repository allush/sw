package main

import (
	"strconv"
	"strings"
)

const DateFormat string = "2006-01-02 15:04:05" //see: https://golang.org/src/time/format.go

type IntSet []int64

func (set *IntSet) add(newItem int64) bool {
	for _, item := range *set {
		if newItem == item {
			return false
		}
	}
	*set = append(*set, newItem)
	return true
}

func (set *IntSet) toString() string {
	ss := []string{}
	for _, item := range *set {
		ss = append(ss, strconv.FormatInt(item, 10))
	}
	return strings.Join(ss, ",")
}
