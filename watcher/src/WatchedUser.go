package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"time"
)

type BaseUserData struct {
	Id         int64
	LastOnline int64
	Online     int8
}

type SocialResponse interface {
	Convert() (*[]BaseUserData, error)
}

type StatusesRefresher interface {
	StatusesLoad() (SocialResponse, error)
	StatusesSave(*sql.DB, SocialResponse) error
}

type WatchedUser struct {
	Id             int64
	Identity       int64
	Network        string
	Online         int8
	LastLoginTime  sql.NullInt64
	LastLogoutTime sql.NullInt64
}

type WatchedUserList struct {
	WatchedUsers []WatchedUser
	Limit        int
}

func (list *WatchedUserList) Append(user WatchedUser) {
	list.WatchedUsers = append(list.WatchedUsers, user)
}

func (list *WatchedUserList) getUniqueIdentities() IntSet {
	set := IntSet{}
	for _, user := range list.WatchedUsers {
		set.add(user.Identity)
	}
	return set
}

func (list WatchedUserList) GetIdentitiesParts() ([]string, error) {

	limit := list.Limit

	uniqueIds := list.getUniqueIdentities()

	uniqueIdsCount := len(uniqueIds)
	if uniqueIdsCount == 0 {
		return []string{}, nil
	}

	first := 0
	last := 0
	if uniqueIdsCount > limit {
		last = limit
	} else {
		last = uniqueIdsCount
	}

	identitiesParts := []string{}

	for {
		part := uniqueIds[first:last]

		identities := part.toString()
		identitiesParts = append(identitiesParts, identities)

		if last == uniqueIdsCount {
			break
		}

		first = last + 1

		if uniqueIdsCount-last > limit {
			last += limit
		} else {
			last = uniqueIdsCount
		}
	}

	return identitiesParts, nil
}

func (list WatchedUserList) StatusesSave(db *sql.DB, response SocialResponse) error {

	socialData, err := response.Convert()
	if err != nil {
		return fmt.Errorf("Failed to convert social response; %s", err)
	}

	query := "INSERT INTO watched_user_event (watched_user_id, event, datetime) VALUES"
	hasChanges := false

	for _, userData := range *socialData {
		for j, watchedUser := range list.WatchedUsers {
			if watchedUser.Identity != userData.Id {
				continue
			}

			if watchedUser.Online == userData.Online {
				continue
			}

			eventTime := time.Now().Unix()
			event := "login"

			if userData.Online == 0 {
				event = "logout"

				warning := ""
				eventTime, warning = watchedUser.getLogoutTime(userData.LastOnline, eventTime)
				if warning != "" {
					fmt.Println(warning)
				}
			}

			list.WatchedUsers[j].updateInList(userData.Online, eventTime)

			err := watchedUser.updateInDb(db, userData.Online, eventTime)
			if err != nil {
				return fmt.Errorf("Failed to update watched user status; %s", err)
			}

			if hasChanges {
				query += ","
			}

			hasChanges = true
			query += "(" + strconv.FormatInt(watchedUser.Id, 10) + ",'" +
				event + "','" + time.Unix(eventTime, 0).Format(DateFormat) + "')"
		}
	}

	if hasChanges {
		_, err := db.Exec(query)
		if err != nil {
			return fmt.Errorf("Failed to insert event; %s", err)
		}
	}

	return nil
}

func (user *WatchedUser) updateInList(online int8, eventTime int64) {
	user.Online = online
	if online == 1 {
		user.LastLoginTime.Int64 = eventTime
	} else {
		user.LastLogoutTime.Int64 = eventTime
	}
}

func (user WatchedUser) updateInDb(db *sql.DB, online int8, eventTime int64) error {

	updateQuery := "UPDATE watched_user SET online = ? "
	if online == 1 {
		updateQuery += ", last_login_time = ? "
	} else {
		updateQuery += ", last_logout_time = ? "
	}
	updateQuery += "WHERE id = ?"

	_, err := db.Exec(
		updateQuery,
		online,
		eventTime,
		user.Id,
	)

	if err != nil {
		return err
	}

	return nil
}

func (user WatchedUser) getLogoutTime(lastSeen int64, eventTime int64) (logoutTime int64, warning string) {
	warning = ""

	// fixme - свойственно для одноклассников, косячок.
	if lastSeen > eventTime {
		warning = fmt.Sprintf("last_online(%s) is from future. now is %s. for user %d\n",
			time.Unix(lastSeen, 0).Format(DateFormat),
			time.Unix(eventTime, 0).Format(DateFormat),
			user.Id)
		lastSeen = eventTime
	}

	if lastSeen < user.LastLoginTime.Int64 {
		warning = fmt.Sprintf("last_online(%s) is before last_login(%s) time for user %d\n",
			time.Unix(lastSeen, 0).Format(DateFormat),
			time.Unix(user.LastLoginTime.Int64, 0).Format(DateFormat),
			user.Id,
		)
	} else {
		warning = fmt.Sprintf("last_online(%s) applied instead of now(%s) time for user %d\n",
			time.Unix(lastSeen, 0).Format(DateFormat),
			time.Unix(eventTime, 0).Format(DateFormat),
			user.Id,
		)
		eventTime = lastSeen
	}

	return eventTime, warning
}
