package main

import (
	"encoding/json"
	"io/ioutil"
)

type Configuration struct {
	Ok struct {
		Id                   string
		UsersPerRequestLimit int
		Access_token         string
		Secret_session_key   string
		Application          struct {
			Id         string
			Public_key string
			Secret_key string
		}
	}

	Fb struct {
		Id                   string
		UsersPerRequestLimit int
		Application          struct {
			Id     string
			Secret string
		}
	}

	Vk struct {
		Id                   string
		UsersPerRequestLimit int
	}

	Dsn string

	MinTimeToRefresh  float64
	ReloadUsersPeriod float64
}

func (config *Configuration) Load(path string) error {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	err = json.Unmarshal(content, config)
	if err != nil {
		return err
	}

	return nil
}
