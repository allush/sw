package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"sync"
	"time"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

type Application struct {
	Config  Configuration
	Db      *sql.DB
	VkUsers VkUserList
	OkUsers OkUserList
	FbUsers FbUserList
	lock    sync.Mutex
}

func (app *Application) load() error {
	var configPath = flag.String("config", "social.conf.json", "config file path")

	flag.Parse()

	err := app.Config.Load(*configPath)
	if err != nil {
		return err
	}

	mysqlHost := os.Getenv("MYSQL_HOST")
	mysqlPort := os.Getenv("MYSQL_PORT")
	mysqlUser := os.Getenv("MYSQL_USER")
	mysqlPassword := os.Getenv("MYSQL_PASSWORD")
	mysqlDatabase := os.Getenv("MYSQL_DATABASE")

	dsn := mysqlUser + ":" + mysqlPassword +
	"@tcp(" + mysqlHost + ":" + mysqlPort + ")/" +
	mysqlDatabase

	app.Db, err = sql.Open("mysql", dsn)
	if err != nil {
		return err
	}

	err = app.Db.Ping()
	if err != nil {
		return err
	}

	return nil
}

func (app *Application) unload() {
	app.Db.Close()
}

func (app *Application) start() {

	msg := make(chan string)
	go app.loadWatchedUsersThread(msg)
	go app.refreshUserStatusesThread(msg)
	for {
		var message string
		message = <-msg
		if message == "crash" {
			return
		}
	}
}

func (app *Application) loadWatchedUsersThread(msg chan string) {
	defer func() { msg <- "crash" }()

	for {
		err := app.loadWatchedUsers()
		if err != nil {
			log.Println(err)
			return
		}

		msg <- "ok"
		time.Sleep(time.Duration(app.Config.ReloadUsersPeriod) * time.Second)
	}
}

func (app *Application) loadWatchedUsers() error {
	app.lock.Lock()
	defer app.lock.Unlock()

	rows, err := app.Db.Query(`
		SELECT id, identity, online, network, last_login_time, last_logout_time
		FROM watched_user
	`)
	if err != nil {
		return err
	}

	app.VkUsers = VkUserList{}
	app.VkUsers.Limit = app.Config.Vk.UsersPerRequestLimit
	app.OkUsers = OkUserList{}
	app.OkUsers.Limit = app.Config.Ok.UsersPerRequestLimit
	app.FbUsers = FbUserList{}
	app.FbUsers.Limit = app.Config.Fb.UsersPerRequestLimit

	for rows.Next() {
		var watchedUser WatchedUser

		err := rows.Scan(
			&watchedUser.Id,
			&watchedUser.Identity,
			&watchedUser.Online,
			&watchedUser.Network,
			&watchedUser.LastLoginTime,
			&watchedUser.LastLogoutTime,
		)

		if err != nil {
			return err
		}

		switch watchedUser.Network {
		case app.Config.Vk.Id:
			app.VkUsers.Append(watchedUser)
		case app.Config.Ok.Id:
			app.OkUsers.Append(watchedUser)
		case app.Config.Fb.Id:
			app.FbUsers.Append(watchedUser)
		default:
			return fmt.Errorf("Unknown network %s;\n", watchedUser.Network)
		}
	}

	return nil
}

func (app *Application) refreshUserStatusesThread(msg chan string) {
	defer func() { msg <- "crash" }()

	for {
		start := time.Now()
		err := app.refreshUserStatuses(
			app.VkUsers,
			app.FbUsers,
			app.OkUsers)

		if err != nil {
			log.Println(err)
			return
		}
		msg <- "ok"

		duration := time.Now().Sub(start).Seconds()
		if duration < app.Config.MinTimeToRefresh {
			time.Sleep(time.Duration(app.Config.MinTimeToRefresh - duration) * time.Second)
		}
	}
}

func (app *Application) refreshUserStatuses(lists ...StatusesRefresher) error {

	for _, list := range lists {
		response, err := list.StatusesLoad()
		if err != nil {
			return err
		}

		app.lock.Lock()
		err = list.StatusesSave(app.Db, response)
		app.lock.Unlock()
		if err != nil {
			return err
		}
	}

	return nil
}
