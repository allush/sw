package main

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

type OkUserList struct {
	WatchedUserList
}

type OkUser struct {
	Uid         string
	Online      string
	Last_online string
}

type OkResponse struct {
	Users []OkUser
}

func (list OkUserList) StatusesLoad() (SocialResponse, error) {

	identitiesParts, err := list.GetIdentitiesParts()
	if err != nil {
		return nil, err
	}

	parsed := OkResponse{}
	for _, identities := range identitiesParts {
		signature := list.calcSignature(identities)

		url := "http://api.ok.ru/api/users/getInfo" +
			"?application_key=" + App.Config.Ok.Application.Public_key +
			"&fields=uid,online,last_online" +
			"&format=json" +
			"&uids=" + identities +
			"&sig=" + signature +
			"&access_token=" + App.Config.Ok.Access_token

		users, err := list.request(url)
		if err != nil {
			return nil, err
		}
		parsed.Users = append(parsed.Users, *users...)
	}

	return parsed, nil
}

func (list OkUserList) request(url string) (*[]OkUser, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	users := []OkUser{}
	err = json.Unmarshal(data, &users)
	if err != nil {
		return nil, err
	}

	return &users, nil
}

func (OkUserList) calcSignature(uids string) string {
	hasher := md5.New()
	str := "application_key=" + App.Config.Ok.Application.Public_key +
		"fields=uid,online,last_onlineformat=jsonuids=" + uids
	hasher.Write([]byte(App.Config.Ok.Access_token + App.Config.Ok.Application.Secret_key))
	str += hex.EncodeToString(hasher.Sum(nil))
	hasher.Reset()
	hasher.Write([]byte(str))
	return hex.EncodeToString(hasher.Sum(nil))
}

func (response OkResponse) Convert() (*[]BaseUserData, error) {
	result := &[]BaseUserData{}

	for _, item := range response.Users {
		uid, err := strconv.ParseInt(item.Uid, 10, 64)
		if err != nil {
			return nil, err
		}

		online := int8(0)
		if item.Online != "" {
			online = 1
		}

		lastOnline, err := time.Parse(DateFormat, item.Last_online)
		if err != nil {
			return nil, err
		}

		*result = append(*result, BaseUserData{
			Id:         uid,
			Online:     online,
			LastOnline: lastOnline.Unix(),
		})
	}

	return result, nil
}
