<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WatchedUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="watched-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'link')->textInput() ?>
    <?= $form->field($model, 'identity')->textInput([
        'readonly' => 'readonly',
    ]) ?>
    <?= $form->field($model, 'avatar_url')->textInput([
        'readonly' => 'readonly',
    ]) ?>
    <?= $form->field($model, 'last_name')->textInput() ?>
    <?= $form->field($model, 'first_name')->textInput() ?>
    <?= $form->field($model, 'network')->dropDownList(\app\components\SocialUser::$networks, [
        'prompt' => '---',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
