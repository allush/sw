<?php
/**
 * @var $this yii\web\View
 * @var $model app\models\WatchedUser
 */

$this->title = 'Создание отслеживаемого пользователя';
$this->params['breadcrumbs'] = [
    ['label' => 'Отслеживаемые пользователи', 'url' => ['index']],
    $this->title,
];

echo $this->render('_form', [
    'model' => $model,
]);
