<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отслеживаемые пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="watched-user-index">
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'avatar_url',
                'format' => 'raw',
                'value' => function (\app\models\WatchedUser $model) {
                    return Html::img($model->avatar_url,[
                        'width' => 80,
                    ]);
                },
            ],
            'last_name',
            'first_name',
            'link',
            'identity',
            'network',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
        ],
    ]); ?>

</div>
