<?php
/**
 * @var $this yii\web\View
 * @var $model app\models\WatchedUser
 */

$this->title = 'Изменить пользователя: ' . ' ' . $model->fullName();

$this->params['breadcrumbs'] = [
    ['label' => 'Отслеживаемые пользователи', 'url' => ['index']],
    'Изменение пользователя ' . $model->fullName(),
];

echo $this->render('_form', [
    'model' => $model,
]);
