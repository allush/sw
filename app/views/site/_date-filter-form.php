<?php
/**
 * @var $this \yii\web\View
 * @var $model DateFilterForm
 */

use app\models\DateFilterForm;
use dosamigos\datetimepicker\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'id' => 'date-filter-form',
    'method' => 'get',
    'action' => ['site/index'],
]); ?>
    <div class="row">
        <div class="col-md-3 col-sm-3">
            <?= $form->field($model, 'period', ['template' => '{input}{error}'])
                ->dropDownList(DateFilterForm::$periodLabels); ?>
        </div>
        <div class="col-md-8 col-sm-8">
            <div id="date-filter-manual" class="form-inline"
                 style="<?= $model->period == DateFilterForm::PERIOD_MANUAL ? '' : 'display: none;'; ?>">
                <?= $form->field($model, 'from', ['template' => '{input}'])->widget(DateTimePicker::classname(), [
                    'language' => 'ru',
                    'size' => 'ms',
                    'pickButtonIcon' => 'glyphicon glyphicon-time',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy HH:ii',
                        'todayBtn' => true,
                    ],
                ]); ?>
                &ndash;
                <?= $form->field($model, 'to', ['template' => '{input}'])->widget(DateTimePicker::classname(), [
                    'language' => 'ru',
                    'size' => 'ms',
                    'pickButtonIcon' => 'glyphicon glyphicon-time',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy HH:ii',
                        'todayBtn' => true,
                    ],
                ]); ?>
                <?= Html::submitButton('Применить', ['class' => 'btn btn-primary']); ?>
            </div>
        </div>
    </div>

<?php $form->end(); ?>