<?php
/**
 * @var $segments []
 * @var $allDuration integer
 */

$duration = $endTime - $startTime;
?>

<div class="chart">
    <?php foreach ($segments as $segment) {
        $width = ($segment['duration'] / $duration) * 100; ?>
        <div class="chart-segment-item <?= $segment['class']; ?>" style="width: <?= $width; ?>%"></div>
    <?php } ?>
</div>
