<?php
/**
 * @var $this \yii\web\View
 * @var $dateFilterForm DateFilterForm
 * @var $addUserModel \app\models\WatchedUser
 */

use app\models\DateFilterForm;

?>

<?= $this->render('_add-user-form', ['model' => $addUserModel]) ?>

<?= $this->render('_date-filter-form', ['model' => $dateFilterForm]) ?>

<?php if (Yii::$app->session->hasFlash('dateFilterFormError')) {
    echo '<div class="alert alert-danger">' .Yii::$app->session->getFlash('dateFilterFormError') . '</div>';
} ?>

<div id="main-chart"
     data-start-date="<?= $dateFilterForm->getStartDate(); ?>"
     data-end-date="<?= $dateFilterForm->getEndDate(); ?>"
     data-period="<?= $dateFilterForm->period; ?>">
    <div class="icon-loading">
        <i class="fa fa-spinner fa-pulse"></i>
    </div>
</div>
