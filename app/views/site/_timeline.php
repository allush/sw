<?php
/**
 * @var $startTime
 * @var $endTime
 */

$duration = $endTime - $startTime;
$hasDate = $duration > 24 * 60 * 60;
$delta = $duration / 8;
?>

<div class="chart-time-line">
    <?php
    $i = 0;
    for ($curTime = $startTime; $curTime <= $endTime; $curTime += $delta) {
        $w = ($i++ / 8) * 100;
        ?>
        <div class="chart-time-item" style="left: <?= $w ?>%">
            <div class="chart-vl"></div>
            <div class="chart-time-item-label"><?= date('H:i', $curTime); ?></div>
            <?php if ($hasDate) {
                echo '<div class="chart-time-item-label">' . date('d.m', $curTime) . '</div>';
            } ?>
        </div>
    <?php } ?>
</div>