<?php
/**
 * @var $this \yii\web\View
 * @var $mailForm \app\models\ContactForm
 * @var $addUserModel \app\models\WatchedUser
 */

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>

<h3>Контакты</h3>

<?php if (Yii::$app->session->hasFlash('mailSendOk')) { ?>
<div class="alert alert-success">
    Письмо успешно отправлено.
</div>
<?php return;
}
if (Yii::$app->session->hasFlash('mailSendError')) { ?>
    <div class="alert alert-error">
        Произошла ошибка при отправке письма.
    </div>
<?php return;
} ?>

<div id="credits">
    <h4>Разработчики: </h4>
    <ul>
        <li>
            <?= Html::a('<strong>Алексей Лушников</strong>', 'http://allush.github.io/'); ?>
        </li>
        <li>
            <?= Html::a('<strong>Алексей Гевондян</strong>', 'http://alexgivi.github.io/'); ?>
        </li>
    </ul>
</div>

<h3>Написать нам письмо</h3>

<div id="mail-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($mailForm, 'name'); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($mailForm, 'email'); ?>
        </div>
    </div>
    <?= $form->field($mailForm, 'subject'); ?>
    <?= $form->field($mailForm, 'body')->textarea(); ?>
    <?= $form->field($mailForm, 'verifyCode')->widget(Captcha::className(), [
        'template' => '<div>{image}{input}</div>',
        'options' => ['class' => 'form-control inline-block'],
        'imageOptions' => ['class' => 'inline-block'],
    ]); ?>
    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']); ?>
    </div>

    <?php $form->end(); ?>
</div>