<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Войдите, чтобы начать пользоваться';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login text-center">
    <div class="site-login-head"><?= Html::encode($this->title) ?></div>
    <script src="//ulogin.ru/js/ulogin.js"></script>
    <div id="uLogin" data-ulogin="display=panel;fields=first_name,last_name;providers=vkontakte,odnoklassniki,mailru,facebook;
    hidden=other;redirect_uri=<?= Url::to(['login']); ?>"></div>
</div>
