<?php
/**
 * @var $this \yii\web\View
 * @var $model \app\models\WatchedUser
 */
?>

<div class="add-user-block">
    <?php $form = \yii\widgets\ActiveForm::begin([
        'id' => 'add-user-form',
        'action' => ['watched-user/create'],
        'enableAjaxValidation' => true,
    ]); ?>
    <?= \yii\helpers\Html::hiddenInput('redirect', Yii::$app->request->url) ?>
    <?= $form->field($model, 'link', ['template' => '{input}'])->textInput([
        'placeholder' => 'Вставьте ссылку на профиль',
    ]) ?>
    <?= $form->field($model, 'avatar_url', ['template' => '{input}'])->hiddenInput() ?>
    <?= $form->field($model, 'network', ['template' => '{input}'])->hiddenInput() ?>
    <?= $form->field($model, 'first_name', ['template' => '{input}'])->hiddenInput() ?>
    <?= $form->field($model, 'last_name', ['template' => '{input}'])->hiddenInput() ?>

    <div class="user-info-loading hidden">
        <i class="fa fa-spinner fa-pulse"></i>
    </div>
    <div class="user-info hidden">
        <img src="" class="user-info__avatar">
        <span class="user-info__first_name"></span>
        <span class="user-info__last_name"></span>
    </div>

    <?= $form->field($model, 'identity', ['template' => '{input}{error}'])->hiddenInput() ?>

    <?= \yii\helpers\Html::submitButton('Добавить', [
        'class' => 'hidden btn btn-default',
        'id' => 'add-user-button',
    ]) ?>
    <?php \yii\widgets\ActiveForm::end(); ?>
</div>
