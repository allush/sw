<?php
/**
 * @var $this \yii\web\View
 * @var $data array
 */
?>

<?php
$startTime = strtotime($data['startDate']);
$endTime = strtotime($data['endDate']);
?>
<?php foreach ($data['chartData'] as $user) {
    echo $this->render('_user', [
        'user' => $user,
        'startTime' => $startTime,
        'endTime' => $endTime,
    ]);
}?>
