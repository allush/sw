<?php
/**
 * @var $this \yii\web\View
 * @var $user []
 * @var $startTime int
 * @var $endTime int
 */
use yii\helpers\Html;

?>

<div class="watched-user" data-network="<?= $user['network'] ?>">
    <div class="row">
        <div class="col-md-1 col-sm-2 col-xs-3">
            <?= Html::img($user['avatar'], [
                'class' => 'user-info__avatar',
            ]) ?>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-9">
            <div><?= $user['name']; ?></div>
            <div><?= $user['onlineDuration']; ?></div>
        </div>
        <div class="col-md-9 col-sm-7 col-xs-12">
            <br class="visible-xs">
            <div class="chart-wrap">
                <?= $this->render('_timeline', [
                    'startTime' => $startTime,
                    'endTime' => $endTime,
                ]) ?>
                <?= $this->render('_chart', [
                    'segments' => $user['segments'],
                    'startTime' => $startTime,
                    'endTime' => $endTime,
                ]) ?>
            </div>
            <br class="visible-xs">
        </div>
    </div>
</div>