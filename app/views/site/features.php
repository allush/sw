<?php
/**
 * @var $this \yii\web\View
 * @var $models \app\models\Feature[]
 */

use yii\helpers\Html;

?>

    <h3>Новые фичи</h3>
    <div>
        На этой странице мы предлагаем вам новые возможности системы,<br>
        а вы голосуете за них, если они вам нравятся.
    </div>
    <br><br>

<?php foreach ($models as $model) {
    $like = $model->getCurrentUserLike();
    $class = $like === null ? '' : ($like == true ? 'like' : 'dislike');
    ?>
    <div class="well feature <?= $class; ?>">
        <div class="row">
            <div class="col-md-8">
                <h3> <?= $model->name; ?></h3>
            </div>
            <div class="col-md-4">
                <?= Html::button('Нужно', [
                    'class' => 'feature-like-button btn btn-success',
                    'data-feature-id' => $model->id,
                    'data-like' => '1',
                    'disabled' => $model->realised_at != null
                ]); ?>
                <?= Html::button('Не нужно', [
                    'class' => 'feature-like-button btn btn-danger',
                    'data-feature-id' => $model->id,
                    'data-like' => '0',
                    'disabled' => $model->realised_at != null
                ]); ?>
            </div>
        </div>
        <?php if ($model->text) { ?>
            <hr>
            <div><?= nl2br($model->text); ?></div>
        <?php } ?>
        <?php if ($model->realised_at) { ?>
            <div class="text-right">
                <i>Реализовано <?= date('d.m.Y', $model->realised_at); ?></i>
            </div>
        <?php } ?>
    </div>
<?php } ?>