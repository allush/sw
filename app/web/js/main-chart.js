/**
 * @param config
 */
function MainChart(config) {
    /**
     * Internal pointer
     * @type {MainChart}
     */
    var _self = this;

    /**
     * Config object
     * UpdatePeriod - time in secs.
     * @type {{autoUpdate: boolean, updatePeriod: number}}
     * @private
     */
    this._config = {
        autoUpdate: false,
        updatePeriod: 60
    };

    /**
     *
     * @param message
     * @private
     */
    this._log = function (message) {
        console.log(message);
    };

    /**
     *
     * @param message
     * @private
     */
    this._error = function (message) {
        throw message;
    };

    this.refreshChart = function () {
        var chartDiv = $('#main-chart');
        var period = chartDiv.attr('data-period');
        var from = chartDiv.attr('data-start-date');
        var to = chartDiv.attr('data-end-date');

        $.ajax({
            url: '/site/get-chart-data',
            type: 'get',
            data: {
                period: period,
                startDate: from,
                endDate: to
            },
            success: function (data) {
                chartDiv.html(data);
                _self._bindTimeLineEvents();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                _self._error(errorThrown);
            }
        });
    };

    this._bindTimeLineEvents = function () {
        if (isMobile()) {
            $('.watched-user').click(function () {
                var elem = $(this).find('.chart-time-line');
                if (!elem.is(':visible')) {
                    elem.show();
                } else {
                    elem.hide();
                }
            });
        } else {
            $('.watched-user')
                .mouseenter(function () {
                    $(this).find('.chart-time-line').show();
                })
                .mouseleave(function () {
                    $(this).find('.chart-time-line').hide();
                })
        }
    };

    /**
     * Init component
     * @private
     */
    this._init = function () {
        if (config) {
            for (var property in config) {
                if (_self._config[property] !== undefined) {
                    _self._config[property] = config[property];
                }
            }
        }

        if (_self._config.autoUpdate === true) {
            _self.refreshChart();
            setInterval(function () {
                _self.refreshChart();
            }, _self._config.updatePeriod * 1000);
        }
    }();
}