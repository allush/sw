function AddUser() {
    /**
     * Internal pointer
     * @type {AddUser}
     * @private
     */
    var _self = this;

    this._watchForm = function () {
        var oldLink = null;
        var userInfo = $('.add-user-block').find('.user-info');
        var userInfoLoading = $('.add-user-block').find('.user-info-loading');
        var addUserButton = $('#add-user-button');
        var validationIdentityMessage = $('.field-watcheduser-identity .help-block');

        setInterval(function () {
            var linkInput = $('#watcheduser-link');
            if (!linkInput.length) {
                return;
            }
            var link = linkInput.val();
            if (oldLink == link) {
                return;
            }
            oldLink = link;

            if (!link.length) {
                addUserButton.addClass('hidden');
                userInfo.addClass('hidden');
                validationIdentityMessage.html('');
                return;
            }

            $.ajax({
                url: '/site/get-user-data',
                dataType: 'json',
                data: {
                    link: link
                },
                beforeSend:  function () {
                    addUserButton.addClass('hidden');
                    userInfo.addClass('hidden');
                    validationIdentityMessage.html('');
                    userInfoLoading.removeClass('hidden');
                },
                success: function (info) {
                    if (!info) {
                        return;
                    }

                    $('#watcheduser-identity').val(info.id);
                    $('#watcheduser-first_name').val(info.first_name);
                    $('#watcheduser-last_name').val(info.last_name);
                    $('#watcheduser-network').val(info.network);
                    $('#watcheduser-avatar_url').val(info.avatar_url);
                    addUserButton.removeClass('hidden');

                    if (userInfo.length) {
                        userInfo.find('.user-info__first_name').html(info.first_name);
                        userInfo.find('.user-info__last_name').html(info.last_name);
                        userInfo.find('.user-info__avatar').attr('src', info.avatar_url);
                        userInfo.removeClass('hidden');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    _self._log(errorThrown)
                },
                complete: function(){
                    userInfoLoading.addClass('hidden');
                }
            });
        }, 200);
    };

    /**
     *
     * @param message
     * @private
     */
    this._log = function (message) {
        console.log(message);
    };

    this._constructor = function () {
        _self._watchForm();
    }();
}
