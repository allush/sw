$(function () {
    var mainChart = null;
    if ($('#main-chart').is('div')) {
        mainChart = new MainChart({autoUpdate: true});
    }

    var addUser = new AddUser();

    $('#date-filter-form').change(function (event) {
        var form = $(this);
        if (event.target.name == 'DateFilterForm[period]') {
            if (event.target.value == '7') {
                $('#date-filter-manual').show();
            } else {
                form.submit();
            }
        }
    });

    $('.feature-like-button').click(function() {
        var button = $(this);
        var feature_id = button.attr('data-feature-id');
        var like = button.attr('data-like');

        $.ajax('/site/like-feature', {
            type: 'post',
            data: {
                id: feature_id,
                like: like
            },
            success: function (data) {
                if (data) {
                    var featureElem = button.parents('.feature');
                    if (like == '1') {
                        featureElem.addClass('like').removeClass('dislike');
                    } else {
                        featureElem.addClass('dislike').removeClass('like');
                    }
                }
            }
        });
    })
});