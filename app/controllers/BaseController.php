<?php

namespace app\controllers;


use yii\web\Controller;

class BaseController extends Controller
{
    public function beforeAction($action)
    {
        if(\Yii::$app->user->isGuest) {
            if($action->getUniqueId() != 'site/login' && $action->id != 'error') {
                $this->redirect(['login']);
            }
        }

        return parent::beforeAction($action);
    }
}