<?php

namespace app\controllers;

use app\components\ChartHelper;
use app\components\OdnoklassnikiSDK;
use app\components\SocialUser;
use app\models\ContactForm;
use app\models\DateFilterForm;
use app\models\Feature;
use app\models\User;
use app\models\WatchedUser;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\swiftmailer\Mailer;
use yii\web\HttpException;

class SiteController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'login' && array_key_exists('token', $_POST)) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $dateFilterForm = new DateFilterForm();
        $dateFilterForm->period = DateFilterForm::PERIOD_REALTIME;
        $dateFilterForm->load(Yii::$app->request->get());
        if (!$dateFilterForm->validate()) {
            Yii::$app->session->setFlash('dateFilterFormError', $dateFilterForm->getErrorsText());
            $dateFilterForm->period = DateFilterForm::PERIOD_REALTIME;
        };

        $addUserModel = new WatchedUser();

        return $this->render('index', [
            'dateFilterForm' => $dateFilterForm,
            'addUserModel' => $addUserModel,
        ]);
    }

    public function actionLogin()
    {
        $this->layout = 'empty';

        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->user->returnUrl);
        }

        if (isset($_POST['token'])) {
            $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
            $data = json_decode($s, true);
            $identity = $data['identity'];
            $user = User::findByIdentity($identity);
            if (!$user) {
                $user = new User();
                $user->attributes = $data;
                if (!$user->save()) {
                    throw new HttpException(500, 'Не удалось создать пользователя');
                }
            }

            if (!Yii::$app->user->login($user, 3600 * 24 * 365)) {
                throw new HttpException(500, 'Не удалось авторизоваться');
            }
            return $this->goHome();
        }

        return $this->render('login');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionFeatures()
    {
        $features = Feature::find()->where('deleted_at is null')->all();

        return $this->render('features', ['models' => $features]);
    }

    public function actionLikeFeature()
    {
        if (!isset($_POST['id'])) {
            Yii::$app->end();
        }

        return Feature::findOne($_POST['id'])->like($_POST['like']);
    }

    public function actionContacts()
    {
        $mailForm = new ContactForm();
        if (Yii::$app->request->post('ContactForm')) {
            $mailForm->load(Yii::$app->request->post());
            if ($mailForm->validate()) {
                $success = Yii::$app->mailer->compose()
                    ->setTo(['alexgivi@nextmail.ru', 'allush@yandex.ru'])
                    ->setFrom([$mailForm->email => $mailForm->name])
                    ->setSubject($mailForm->subject)
                    ->setTextBody($mailForm->body)
                    ->send();

                if ($success) {
                    Yii::$app->session->setFlash('mailSendOk');
                } else {
                    Yii::$app->session->setFlash('mailSendError');
                }
            }
        }
        return $this->render('contacts', ['mailForm' => $mailForm]);
    }

    /**
     * Получить данные для построения графика
     * главной страницы
     *
     * @param int $period
     * @param $startDate
     * @param $endDate
     * @return string
     */
    public function actionGetChartData($period = DateFilterForm::PERIOD_REALTIME, $startDate = null, $endDate = null)
    {
        $dateFilterForm = new DateFilterForm();
        $dateFilterForm->period = $period;
        $dateFilterForm->from = $startDate;
        $dateFilterForm->to = $endDate;

        $helper = new ChartHelper();
        return $this->renderPartial('_users', [
            'data' => $helper->getChartData($dateFilterForm->getStartDate(), $dateFilterForm->getEndDate())
        ]);
    }

    public function actionGetUserData($link)
    {
        return SocialUser::fromLink($link)->getInfo();
    }

}
