<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $identity
 * @property string $auth_key
 * @property string $role
 *
 * @property WatchedUser[] $watchedUsers
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{

    const ROLE_ADMIN = 'admin';
    const ROLE_GUEST = 'guest';

    public static $roles = [
        self::ROLE_GUEST => 'Гость',
        self::ROLE_ADMIN => 'Администратор'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'identity', 'auth_key', 'role'], 'string', 'max' => 255],
            [['identity'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'identity' => 'Идентификатор',
            'auth_key' => 'Ключ аутентификации',
            'role' => 'Роль',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param $identity
     * @return null|static
     */
    public static function findByIdentity($identity)
    {
        return self::findOne(['identity' => $identity]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key == $authKey;
    }

    public function getName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWatchedUsers()
    {
        return $this->hasMany(WatchedUser::className(), ['user_id' => 'id']);
    }

    public function isAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }
}
