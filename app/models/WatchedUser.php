<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $identity
 * @property string $network
 * @property string $first_name
 * @property string $last_name
 * @property string $link
 * @property string $avatar_url
 *
 * @property User $user
 */
class WatchedUser extends ActiveRecord
{
    public static function tableName()
    {
        return 'watched_user';
    }

    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['identity', 'first_name', 'last_name', 'network'], 'required'],
            [['identity', 'first_name', 'last_name', 'network'], 'string', 'max' => 255],
            [['link', 'avatar_url'], 'url'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => '#',
            'user_id' => 'Пользователь',
            'identity' => 'Идентификатор',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'network' => 'Сеть',
            'link' => 'Ссылка',
            'avatar_url' => 'Аватар',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function beforeValidate()
    {
        $this->user_id = Yii::$app->user->id;

        if ($this->isNewRecord) {
            $alreadyExists = 0 < (int)WatchedUser::find()->where([
                    'user_id' => $this->user_id,
                    'identity' => $this->identity,
                ])->count();

            if ($alreadyExists) {
                $this->addError('identity', 'Вы уже следите за данным пользователем');
            }
        }

        return parent::beforeValidate();
    }

    public function beforeDelete()
    {
        WatchedUserEvent::deleteAll([
            'watched_user_id' => $this->id,
        ]);

        return parent::beforeDelete();
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
