<?php

namespace app\models;

class DateFilterForm extends \yii\base\Model
{
    public $period;
    public $from;
    public $to;

    const PERIOD_REALTIME = 1;
    const PERIOD_TODAY = 2;
    const PERIOD_YESTERDAY = 3;
    const PERIOD_WEEK = 4;
    const PERIOD_MONTH = 5;
    const PERIOD_ALL = 6;
    const PERIOD_MANUAL = 7;

    public static $periodLabels = [
        self::PERIOD_REALTIME => 'Последний час',
        self::PERIOD_TODAY => 'Сегодня',
        self::PERIOD_YESTERDAY => 'Вчера',
        self::PERIOD_WEEK => 'Последняя неделя',
        self::PERIOD_MONTH => 'Последний месяц',
//        self::PERIOD_ALL => 'Все время',
        self::PERIOD_MANUAL => 'Произвольный',
    ];

    public function attributeLabels()
    {
        return [
            'period' => 'Период',
            'from' => 'Начало',
            'to' => 'Конец',
        ];
    }

    public function rules()
    {
        return [
            ['period', 'required'],
            ['period', 'integer', 'integerOnly' => true],
            [['from', 'to'], 'date', 'format' => 'php:d.m.Y H:i']
        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        if ($this->period == self::PERIOD_MANUAL) {
            if (!$this->from) {
                $this->addError('from', 'Необходимо указать начало периода');
            }
            if (!$this->to) {
                $this->addError('to', 'Необходимо указать конец периода');
            }
            if ($this->from && $this->to && strtotime($this->from) > strtotime($this->to)) {
                $this->addError('from', 'Начало периода должно быть раньше конца');
            }
        }
        return parent::validate($attributeNames, false);
    }

    public function getStartDate($format = 'Y-m-d H:i:s')
    {
        switch ($this->period) {
            case self::PERIOD_REALTIME: return date($format, time() - 3600);
            case self::PERIOD_TODAY: return date($format, strtotime('today'));
            case self::PERIOD_YESTERDAY: return date($format, strtotime('yesterday'));
            case self::PERIOD_WEEK: return date($format, time() - 3600 * 24 * 7);
            case self::PERIOD_MONTH: return date($format, time() - 3600 * 24 * 30);
            case self::PERIOD_MANUAL: return date($format, strtotime($this->from));
        }
        return null;
    }

    public function getEndDate($format = 'Y-m-d H:i:s')
    {
        switch ($this->period) {
            case self::PERIOD_TODAY: return date($format, strtotime('today 24:00'));
            case self::PERIOD_YESTERDAY: return date($format, strtotime('yesterday 24:00'));
            case self::PERIOD_MANUAL: return date($format, strtotime($this->to));
        }

        return date($format, time());
    }

    public function getErrorsText()
    {
        $result = "";
        foreach ($this->errors as $errorList) {
            foreach ($errorList as $error) {
                $result .= $error . '<br>';
            }
        }
        return $result;
    }
}