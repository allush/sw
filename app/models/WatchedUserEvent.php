<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "watched_user_event".
 *
 * @property integer $id
 * @property integer $watched_user_id
 * @property string $event
 * @property string $datetime
 *
 * @property WatchedUser $watchedUser
 */
class WatchedUserEvent extends \yii\db\ActiveRecord
{
    const EVENT_LOGIN = 'login';
    const EVENT_LOGOUT = 'logout';

    public static $eventLabels = [
        self::EVENT_LOGIN => 'Вход в систему',
        self::EVENT_LOGOUT => 'Выход из системы'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'watched_user_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['watched_user_id'], 'required'],
            [['watched_user_id'], 'integer'],
            [['event'], 'string'],
            [['datetime'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'watched_user_id' => 'Watched User ID',
            'event' => 'Event',
            'datetime' => 'Datetime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWatchedUser()
    {
        return $this->hasOne(WatchedUser::className(), ['id' => 'watched_user_id']);
    }

    /**
     * найти последнее событие, произошедшее до указанного времени
     * @param $watchedUserID int идентификатор отслеживаемого пользователя
     * @param $beforeDate string дата и время
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findLastEvent($watchedUserID, $beforeDate)
    {
        return static::findBySql(
            'SELECT * FROM watched_user_event
                 WHERE datetime < :start AND watched_user_id = :user
                 ORDER BY id DESC LIMIT 1', [
            'start' => $beforeDate,
            'user' => $watchedUserID
        ])->one();
    }
}
