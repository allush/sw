<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feature".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property integer $deleted_at
 * @property integer $realised_at
 *
 * @property FeatureLike[] $featureLikes
 */
class Feature extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feature';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['text'], 'string'],
            [['deleted_at', 'realised_at'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'text' => 'Описание',
            'deleted_at' => 'Удалена',
            'realised_at' => 'Реализована',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatureLikes()
    {
        return $this->hasMany(FeatureLike::className(), ['feature_id' => 'id']);
    }

    public function delete()
    {
        $this->deleted_at = time();
        return $this->save();
    }

    /**
     * @param bool|true $like
     * @return bool
     */
    public function like($like = true)
    {
        /** @var FeatureLike $featureLike */
        $featureLike = FeatureLike::findOne([
            'feature_id' => $this->id,
            'user_id' => Yii::$app->user->id
        ]);
        if ($featureLike == null) {
            $featureLike = new FeatureLike();
            $featureLike->feature_id = $this->id;
            $featureLike->user_id = Yii::$app->user->id;
        } elseif ($featureLike->like == $like) {
            return false;
        }

        $featureLike->date = date('Y-m-d');
        $featureLike->like = $like;

        return $featureLike->save();
    }

    /**
     * @return bool|null
     */
    public function getCurrentUserLike()
    {
        foreach ($this->featureLikes as $like) {
            if ($like->user_id == Yii::$app->user->id) {
                return $like->like;
            }
        }
        return null;
    }

    public function getAllLikesCount()
    {
        $count = 0;
        foreach ($this->featureLikes as $like) {
            if ($like->like) {
                $count ++;
            }
        }
        return $count;
    }

    public function getAllDislikesCount()
    {
        $count = 0;
        foreach ($this->featureLikes as $like) {
            if (!$like->like) {
                $count ++;
            }
        }
        return $count;
    }
}
