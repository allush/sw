<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feature_like".
 *
 * @property integer $id
 * @property integer $feature_id
 * @property integer $user_id
 * @property string $date
 * @property boolean $like
 *
 * @property User $user
 * @property Feature $feature
 */
class FeatureLike extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feature_like';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feature_id', 'user_id', 'like'], 'required'],
            [['feature_id', 'user_id'], 'integer'],
            [['like'], 'boolean'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'feature_id' => 'Фича',
            'user_id' => 'Пользователь',
            'date' => 'Дата',
            'like' => 'Нравится',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeature()
    {
        return $this->hasOne(Feature::className(), ['id' => 'feature_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
