<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 14.10.15
 * Time: 15:35
 */

namespace app\components;

use Yii;
use yii\base\Exception;

class Logger
{
    public static function error($message)
    {
        if (defined('YII_ENV') && YII_ENV == 'dev') {
            if (is_object($message)) {
                throw $message;
            }
            throw new Exception($message);
        } else {
            Yii::error($message);
        }
    }
}