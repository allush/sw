<?php
namespace app\components;

use yii\helpers\BaseUrl;
use yii\helpers\Url;

class SocialUser
{
    public $network = null;
    public $identity = null;

    const NETWORK_VK = 'vkontakte';
    const NETWORK_OK = 'odnoklassniki';
    const NETWORK_FB = 'facebook';

    const FIELD_ID = 'id';
    const FIELD_FIRST_NAME = 'first_name';
    const FIELD_LAST_NAME = 'last_name';
    const FIELD_NETWORK = 'network';
    const FIELD_AVATAR = 'avatar_url';

    public static $networks = [
        self::NETWORK_VK => 'Вконтакте',
        self::NETWORK_FB => 'Facebook',
        self::NETWORK_OK => 'Одноклассники',
    ];

    /**
     * @param $network
     * @param $identity
     */
    public function __construct($network, $identity)
    {
        $this->network = $network;
        $this->identity = $identity;
    }

    public static function fromLink($link)
    {
        $network = null;
        $identity = null;
        if (strpos($link, 'vk.com')) {
            $network = self::NETWORK_VK;

            $link = str_replace('http://vk.com/', '', $link);
            $link = str_replace('https://vk.com/', '', $link);
            $identity = $link;
        } elseif (strpos($link, 'ok.ru')) {
            $network = self::NETWORK_OK;
            $link = str_replace('http://ok.ru/profile/', '', $link);
            $identity = $link;
        }

        return new static($network, $identity);
    }

    public function getInfo()
    {
        $info = '';
        switch ($this->network) {
            case self::NETWORK_VK:
                $info = $this->getVkUserInfo();
                break;
            case self::NETWORK_OK:
                $info = $this->getOkUserInfo();
                break;
        }

        return $info;
    }

    private function getResponse($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    private function getVkUserInfo()
    {
        $v = '5.37';
        $fields = [
            'photo_200',
        ];

        $url = 'https://api.vk.com/method/users.get?' .
            'user_ids=' . $this->identity .
            '&v=' . $v .
            '&fields=' . implode($fields);

        $response = $this->getResponse($url);

        $data = json_decode($response, true);
        $data = $data['response'][0];
        $data[self::FIELD_NETWORK] = self::NETWORK_VK;
        $data[self::FIELD_AVATAR] = $data['photo_200'];

        return json_encode($data);
    }

    private function getFbUserInfo()
    {
        return '{}';
    }

    private function calculateOkSignature($params, $secret)
    {
        ksort($params);
        $paramsString = '';
        foreach ($params as $key => $value) {
            $paramsString .= $key . '=' . $value;
        }
        $paramsString .= $secret;
        return mb_strtolower(md5($paramsString));
    }

    private function getOkUserInfo()
    {
        $config = json_decode(file_get_contents(\Yii::$app->basePath . '/config/social.conf.json'), true);
        $config = $config['ok'];

        $params = [
            'application_key' => $config['application']['public_key'],
            'format' => 'json',
            'uids' => $this->identity,
            'fields' => 'uid,first_name,last_name,pic_5'
        ];
        $params['sig'] = $this->calculateOkSignature(
            $params,
            md5($config['access_token'] . $config['application']['secret_key'])
        );
        $params['access_token'] = $config['access_token'];

        $url = 'http://api.ok.ru/api/users/getInfo?' . http_build_query($params);
        $response = $this->getResponse($url);
        $data = json_decode($response, true);
        $data = $data[0];
        $data[self::FIELD_NETWORK] = self::NETWORK_OK;
        $data[self::FIELD_ID] = $data['uid'];
        $data[self::FIELD_AVATAR] = $data['pic_5'];

        return json_encode($data);
    }
}
