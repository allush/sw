<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/8/15
 * Time: 5:52 AM
 */

namespace app\components;

use app\models\WatchedUser;
use app\models\WatchedUserEvent;
use Yii;

class ChartHelper
{
    /**
     * получить время самого первого события входа или выхода
     * среди отслеживаемых пользователей для текущего пользователя системы
     * @return bool|null|string время первого события
     */
    public function getFirstEventTime()
    {
        $sql = '
            SELECT MIN(datetime)
            FROM watched_user_event
            INNER JOIN watched_user ON watched_user_event.watched_user_id = watched_user.id
            WHERE watched_user.user_id = :user_id';

        return Yii::$app->db->createCommand($sql, [
            ':user_id' => Yii::$app->user->id,
        ])->queryScalar();
    }

    /**
     * получить данные для построения графика
     * для всех отслеживаемых пользователей текущего вошедшего в систему пользователя
     * за указанный период времени
     * @param string $startDate время начала отслеживаемого интервала
     * @param string $endDate время окончания отслеживаемого интервала
     * @return string
     */
    public function getChartData($startDate = null, $endDate = null)
    {
        if (!$startDate) {
            $startDate = $this->getFirstEventTime();
        }

        $sql = '
            SELECT event.* FROM watched_user_event event
            INNER JOIN watched_user ON event.watched_user_id = watched_user.id
            WHERE
                watched_user.user_id = :user_id AND
                datetime >= :start AND
                datetime <= :end';
        $params = [
            ':user_id' => Yii::$app->user->id,
            ':start' => $startDate,
            ':end' => $endDate,
        ];

        /** @var WatchedUserEvent[] $watchedUserEvents */
        $watchedUserEvents = WatchedUserEvent::findBySql($sql, $params)->all();

        /**
         * массив данных для графика. пользователь-событие-время
         */
        $data = [];
        foreach ($watchedUserEvents as $event) {
            $data[$event->watched_user_id][$event->event][] = $event->datetime;
        }

        $absentWatchedUsers = WatchedUser::findAll(['user_id' => Yii::$app->user->id]);
        foreach ($absentWatchedUsers as $watchedUser) {
            if (!isset($data[$watchedUser->id])) {
                $data[$watchedUser->id] = [];
            }
        }

        $dataProvider = [];
        foreach ($data as $watched_user_id => $events) {
            /** @var WatchedUserEvent $lastEvent */
            $lastEvent = WatchedUserEvent::findLastEvent($watched_user_id, $startDate);
            /** @var WatchedUser $watchedUser */
            $watchedUser = WatchedUser::findOne($watched_user_id);

            $dataProvider[] = $this->calculateDataProvider(
                $watchedUser,
                $lastEvent === null ? null : $lastEvent->event == 'login',
                isset($events['login']) ? $events['login'] : [],
                isset($events['logout']) ? $events['logout'] : [],
                strtotime($startDate),
                strtotime($endDate)
            );
        }


        return [
            'chartData' => $dataProvider,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ];
    }

    private function getClass($status)
    {
        if ($status === null) {
            return 'no-status';
        }
        return $status ? 'online' : 'offline';
    }

    /**
     * @param $watchedUser WatchedUser
     * @param $beforeState bool | null
     * @param $logins array
     * @param $logouts array
     * @param $startTime integer
     * @param $endTime integer
     * @return array
     */
    private function calculateDataProvider($watchedUser, $beforeState, $logins, $logouts, $startTime, $endTime)
    {
        $time = time();
        $data = [
            0 => $beforeState,
        ];
        foreach ($logins as $date) {
            $data[strtotime($date) - $startTime] = true;
        }
        foreach ($logouts as $date) {
            $data[strtotime($date) - $startTime] = false;
        }

        if ($endTime > $time) {
            $data[$time - $startTime] = null;
        }

        $data[$endTime - $startTime] = null;

        ksort($data);

        $segments = [];
        $onlineDuration = null;
        $prevTime = 0;
        $prevStatus = null;
        foreach ($data as $time => $status) {
            if ($time) {
                $segments[] = [
                    'duration' => $time - $prevTime,
                    'class' => $this->getClass($prevStatus),
                ];
            }
            if ($prevStatus) {
                $onlineDuration += $time - $prevTime;
            }
            $prevTime = $time;
            $prevStatus = $status;
        }

        return [
            'name' => $watchedUser->fullName(),
            'avatar' => $watchedUser->avatar_url,
            'network' => $watchedUser->network,
            'segments' => $segments,
            'onlineDuration' => $this->formatOnlineDuration($endTime - $startTime, $onlineDuration),
        ];

    }

    private function formatOnlineDuration($allDuration, $duration)
    {
        if ($duration === null) {
            return 'Нет данных';
        }
        if ($duration == 0) {
            return '';
        }

        $durationPercent = (int)(($duration / $allDuration) * 100) . '%';

        $durationAbsolute = $this->getAbsoluteDuration($duration);
        if ($durationAbsolute) {
            $durationPercent .= ' (' . $durationAbsolute . ')';
        }

        return $durationPercent;
    }

    private function getAbsoluteDuration($duration)
    {
        $days = (int)($duration / (3600 * 24));
        $hours = (int)(($duration - ($days * 3600 * 24)) / 3600);
        $minutes = (int)(($duration - ($days * 3600 * 24) - ($hours * 3600)) / 60);

        $durationAbsolute = [];
        if ($days) {
            $durationAbsolute[] = $days . ' д.';
        }
        if ($hours) {
            $durationAbsolute [] = $hours . ' ч.';
        }
        if ($minutes) {
            $durationAbsolute [] = $minutes . ' мин.';
        }

        return implode(' ', $durationAbsolute);
    }
}