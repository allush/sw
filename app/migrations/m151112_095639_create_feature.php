<?php

use yii\db\Schema;
use yii\db\Migration;

class m151112_095639_create_feature extends Migration
{
    public function up()
    {
        $this->createTable('feature', [
            'id' => 'pk',
            'name' => 'string',
            'text' => 'text',
            'deleted_at' => 'integer'
        ]);

        $this->createTable('feature_like', [
            'id' => 'pk',
            'feature_id' => 'integer not null',
            'user_id' => 'integer not null',
            'date' => 'date not null'
        ]);

        $this->addForeignKey('feature_like_feature_id_fk', 'feature_like', 'feature_id', 'feature', 'id');
        $this->addForeignKey('feature_like_user_id_fk', 'feature_like', 'user_id', 'user', 'id');

        return true;
    }

    public function down()
    {
        $this->dropForeignKey('feature_like_user_id_fk', 'feature_like');
        $this->dropForeignKey('feature_like_feature_id_fk', 'feature_like');
        $this->dropTable('feature_like');
        $this->dropTable('feature');
        return true;
    }

}
