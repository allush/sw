<?php

use yii\db\Schema;
use yii\db\Migration;

class m151021_070301_change_watched_user_identity_type extends Migration
{
    public function up()
    {
        $this->alterColumn('watched_user', 'identity', 'int(10) unsigned not null');

        return true;
    }

    public function down()
    {
        $this->alterColumn('watched_user', 'identity', 'string not null');

        return true;
    }
}
