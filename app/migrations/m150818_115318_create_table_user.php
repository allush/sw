<?php

use yii\db\Schema;
use yii\db\Migration;

class m150818_115318_create_table_user extends Migration
{
    public function up()
    {
        $this->createTable('user',[
            'id' => 'pk',
            'first_name' => 'string',
            'last_name' => 'string',
            'identity' => 'string',
        ]);
        return true;
    }

    public function down()
    {
        $this->dropTable('user');
        return true;
    }

}
