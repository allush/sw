<?php

use yii\db\Schema;
use yii\db\Migration;

class m151021_035125_add_user_watched_user_create_time extends Migration
{
    public function up()
    {
        $this->addColumn('user_watched_user', 'created_at', 'timestamp');

        return true;
    }

    public function down()
    {
        $this->dropColumn('user_watched_user', 'created_at');

        return true;
    }
}
