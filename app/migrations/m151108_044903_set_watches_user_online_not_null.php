<?php

use yii\db\Schema;
use yii\db\Migration;

class m151108_044903_set_watches_user_online_not_null extends Migration
{
    public function up()
    {
        $this->execute('UPDATE watched_user SET online=0 WHERE online IS NULL');
        $this->alterColumn('watched_user', 'online', 'boolean not null default 0');

        return true;
    }

    public function down()
    {
        $this->alterColumn('watched_user', 'online', 'boolean');

        return true;
    }
}
