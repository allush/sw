<?php

use yii\db\Schema;
use yii\db\Migration;

class m151113_112122_add_dislike_and_realised extends Migration
{
    public function up()
    {
        $this->addColumn('feature_like', 'like', 'boolean not null');
        $this->addColumn('feature', 'realised_at', 'integer');
        return true;
    }

    public function down()
    {
        $this->dropColumn('feature', 'realised_at');
        $this->dropColumn('feature_like', 'like');
        return true;
    }

}
