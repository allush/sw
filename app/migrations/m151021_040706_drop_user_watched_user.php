<?php

use yii\db\Schema;
use yii\db\Migration;

class m151021_040706_drop_user_watched_user extends Migration
{
    public function up()
    {
        $this->truncateTable('user_watched_user');
        $this->truncateTable('watched_user_event');
        $this->delete('user');
        $this->delete('watched_user');

        $this->addColumn('watched_user', 'user_id', 'int not null AFTER id');
        $this->addForeignKey('watched_user_user_id_FK', 'watched_user', 'user_id', 'user', 'id');

        $this->dropTable('user_watched_user');

        return true;
    }

    public function down()
    {
        echo "m151021_040706_drop_user_watched_user cannot be reverted.\n";

        return false;
    }
}
