<?php

use yii\db\Schema;
use yii\db\Migration;

class m151016_122114_add_user_auth_key extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'auth_key', 'string');
        return true;
    }

    public function down()
    {
        $this->dropColumn('user', 'auth_key');
        return true;
    }
}
