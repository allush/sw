<?php

use yii\db\Schema;
use yii\db\Migration;

class m150818_123212_create_table_watched_user extends Migration
{
    public function up()
    {
        $this->createTable('watched_user', [
            'id' => 'pk',
            'identity' => 'string'
        ]);

        $this->createTable('user_watched_user', [
            'id' => 'pk',
            'user_id' => 'integer not null',
            'watched_user_id' => 'integer not null'
        ]);
        $this->addForeignKey('user_watched_user_user_id_fk', 'user_watched_user', 'user_id', 'user', 'id');
        $this->addForeignKey('user_watched_user_watched_user_id_fk', 'user_watched_user', 'watched_user_id', 'watched_user', 'id');

        return true;
    }

    public function down()
    {
        $this->dropForeignKey('user_watched_user_user_id_fk', 'user_watched_user');
        $this->dropForeignKey('user_watched_user_watched_user_id_fk', 'user_watched_user');
        $this->dropTable('user_watched_user');
        $this->dropTable('watched_user');
        return true;
    }
}
