<?php

use yii\db\Schema;
use yii\db\Migration;

class m151118_120403_rewrite_admin extends Migration
{
    public function up()
    {
        $this->dropTable('admin');
        $this->addColumn('user', 'role', 'string');
        return true;
    }

    public function down()
    {
        echo "m151118_120403_rewrite_admin cannot be reverted.\n";

        return false;
    }
}
