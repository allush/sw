<?php

use yii\db\Schema;
use yii\db\Migration;

class m151016_114357_add_new_fields_to_watched_user extends Migration
{
    public function up()
    {
        $this->addColumn('watched_user', 'network', 'string not null');
        $this->addColumn('watched_user', 'first_name', 'string not null');
        $this->addColumn('watched_user', 'last_name', 'string not null');
        $this->addColumn('watched_user', 'link', 'string not null');

        return true;
    }

    public function down()
    {
        $this->dropColumn('watched_user', 'network');
        $this->dropColumn('watched_user', 'first_name');
        $this->dropColumn('watched_user', 'last_name');
        $this->dropColumn('watched_user', 'link');

        return true;
    }
}
