<?php

use yii\db\Schema;
use yii\db\Migration;

class m150930_115724_create_watched_user_event extends Migration
{
    public function up()
    {
        $this->createTable('watched_user_event', [
            'id' => 'pk',
            'watched_user_id' => 'integer not null',
            'event' => "enum('login', 'logout')",
            'datetime' => 'timestamp'
        ]);
        $this->addForeignKey('watched_user_event_watched_user_id_fk',
            'watched_user_event', 'watched_user_id', 'watched_user', 'id');
        return true;
    }

    public function down()
    {
        $this->dropForeignKey('watched_user_event_watched_user_id_fk', 'watched_user_event');
        $this->dropTable('watched_user_event');
        return true;
    }

}
