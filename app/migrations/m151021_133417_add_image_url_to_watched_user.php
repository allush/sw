<?php

use yii\db\Schema;
use yii\db\Migration;

class m151021_133417_add_image_url_to_watched_user extends Migration
{
    public function up()
    {
        $this->addColumn('watched_user', 'avatar_url', 'string');
        return true;
    }

    public function down()
    {
        $this->dropColumn('watched_user', 'avatar_url');
        return true;
    }
}
