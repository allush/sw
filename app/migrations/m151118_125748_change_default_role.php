<?php

use yii\db\Schema;
use yii\db\Migration;

class m151118_125748_change_default_role extends Migration
{
    public function up()
    {
        $this->alterColumn('user', 'role', 'string not null default "guest"');
        return true;
    }

    public function down()
    {
        $this->alterColumn('user', 'role', 'string');
        return true;
    }
}
