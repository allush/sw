<?php

use yii\db\Schema;
use yii\db\Migration;

class m151102_194057_change_identity_length extends Migration
{
    public function up()
    {
        $this->alterColumn('watched_user', 'identity', 'bigint not null');
        return true;
    }

    public function down()
    {
        $this->alterColumn('watched_user', 'identity', 'integer not null');
        return true;
    }

}
