<?php

use yii\db\Schema;
use yii\db\Migration;

class m151002_092249_add_watched_user_online extends Migration
{
    public function up()
    {
        $this->addColumn('watched_user', 'online', 'boolean');
        return true;
    }

    public function down()
    {
        $this->dropColumn('watched_user', 'online');
        return true;
    }

}
