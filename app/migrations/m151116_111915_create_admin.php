<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_111915_create_admin extends Migration
{
    public function up()
    {
        $this->createTable('admin', [
            'id' => 'pk',
            'email' => 'string not null',
            'password' => 'string not null'
        ]);
        return true;
    }

    public function down()
    {
        $this->dropTable('admin');
        return true;
    }
}
