<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_100428_add_last_events extends Migration
{
    public function up()
    {
        $this->addColumn('watched_user', 'last_login_time', 'integer');
        $this->addColumn('watched_user', 'last_logout_time', 'integer');
        return true;
    }

    public function down()
    {
        $this->dropColumn('watched_user', 'last_login_time');
        $this->dropColumn('watched_user', 'last_logout_time');
        return true;
    }
}
