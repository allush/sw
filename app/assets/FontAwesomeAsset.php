<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@vendor/fortawesome/font-awesome';
    public $js = [];
    public $css = [
        'css/font-awesome.min.css',
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}