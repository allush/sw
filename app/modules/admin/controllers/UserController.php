<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 11/16/15
 * Time: 1:48 PM
 */

namespace app\modules\admin\controllers;


use app\models\User;
use app\modules\admin\models\Admin;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class UserController extends BackendController
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
           'query' => User::find()
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionMenu()
    {
        return $this->render('menu');
    }

    public function actionAccessDenied()
    {
        return $this->render('access-denied');
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id int
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}