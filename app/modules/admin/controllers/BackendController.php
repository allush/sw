<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 11/16/15
 * Time: 1:48 PM
 */

namespace app\modules\admin\controllers;


use Yii;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\Controller;

class BackendController extends Controller
{
    public function beforeAction($action)
    {
        $this->view->params['breadcrumbs'] = [['url' => '/admin/', 'label' => 'Админка']];

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/login']);
        }
        if (!Yii::$app->user->identity->isAdmin()  && $action->id != 'access-denied') {
            return $this->redirect(['/admin/user/access-denied']);
        }
        return parent::beforeAction($action);
    }
}