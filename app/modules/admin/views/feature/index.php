<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фичи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feature-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать фичу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'text:ntext',
            [
                'attribute' => 'allLikesCount',
                'label' => 'Всего лайков'
            ],
            [
                'attribute' => 'allDislikesCount',
                'label' => 'Всего дислайков'
            ],
            [
                'class' => \yii\grid\ActionColumn::className(),
                'template' => '{realize}',
                'header' => 'Реализовано',
                'buttons' => [
                    'realize' => function ($url, $model, $key) {
                        return Html::a($model->realised_at ? date('d.m.Y', $model->realised_at) : 'Нет',
                            ['realize', 'id' => $model->id]);
                    }
                ]
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>

</div>
