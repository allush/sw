<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Feature */

$this->title = 'Изменение фичи: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Фичи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="feature-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
