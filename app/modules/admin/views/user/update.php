<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Изменение пользователя: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="feature-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'role')->dropDownList(User::$roles); ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
