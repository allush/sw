<?php

/**
 * @var $this \yii\web\View
 */
?>

<h3>Ошибка доступа</h3>
<div>
    У вас нет прав администратора. Обратитесь к администраторам, если хотите получить их.
</div>