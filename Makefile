ENV ?= local

build:
	docker run \
		--rm \
		-e GOPATH=/go \
		-v "`pwd`/watcher:/app" \
		-v "`pwd`/watcher/src:/go/src/app" \
		-v "`pwd`/watcher/build/go:/go" \
		-w /go/src/app \
		idfly/go-app \
			bash -c "\
				go get .; \
				go build -o /app/build/watcher \
			"

	docker run \
		--rm \
		-v "`pwd`/app":/app \
		-v "`pwd`/composer/auth.json":/root/.composer/auth.json \
		allush/yii-app \
			bash -c "\
				composer self-update; \
				composer global require fxp/composer-asset-plugin; \
				composer install; \
				chmod -R 777 web/assets; \
				chmod -R 777 runtime \
			"
start:
	docker-compose -f config/${ENV}/main.yml up -d \
		--force-recreate \
		--remove-orphans

	while true; do 															\
		docker exec app.sw.${ENV} ./yii migrate/history >/dev/null 2>&1;	\
		if [ $${?} = 0 ] ; then												\
			break;															\
		fi;																	\
		echo -n . ;															\
		sleep 1;															\
	done

stop:
	docker-compose -f config/${ENV}/main.yml stop

ps:
	docker-compose -f config/${ENV}/main.yml ps

logs:
	docker-compose -f config/${ENV}/main.yml logs

migrate:
	docker exec app.sw.${ENV} ./yii migrate --interactive=0

up:
	make build

	make start

	make migrate

	# Приложение запущено!
	#
	# 1. Откройте http://localhost в браузере для доступа к приложению
	# 2. Выполните "make logs" для подключения к логам
	# 3. Выполните "make stop" для остановки приложения
